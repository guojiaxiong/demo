package handler

import (
   "code.byted.org/middleware/hertz/pkg/app"
   "code.byted.org/middleware/hertz/pkg/common/utils"
   // 以下部分import需要根据自己的路径进行相应的改动
   "code.byted.org/demo/http/caller"
   "code.byted.org/demo/http/kitex_gen/huhangtian/demo/rpc"
	"context"
)

func Rpc (ctx context.Context, c *app.RequestContext) {
   var err error
   // 这里也不要一股脑复制，用rpc.xxx是因为引入的文件路径下的包名为rpc，如果用了自定义的名字那么这边包名也需要改
   // 这边是封装请求参数
   // 在这边传的这个参数12345在服务端没做逻辑处理，就传着玩儿，体现一下传参流程
   rpcReq := &rpc.GetItemRequest{Id: 12345}
   // 这边用rpcReq去请求并且得到返回值
   // 当前版本的hertz已对RequestContext实现context.Context接口，故无需再用c.Context()获取
   // rpcResp, err := caller.ItemClient.GetItem(c.Context(), rpcReq)
   rpcResp, err := caller.ItemClient.GetItem(ctx, rpcReq)
   if err != nil {
      c.JSON(200, utils.H{
         "error": err.Error(),
      })
   } else {
      c.JSON(200, utils.H{
         "rpcResp": rpcResp.Item,
      })
   }
}
