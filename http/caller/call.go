package caller

import (
    "code.byted.org/kite/kitex/client"
    // 这边的路径名需要注意按照自己前面定义的来 对应上
    "code.byted.org/demo/http/kitex_gen/huhangtian/demo/rpc/itemservice"
)

var ItemClient itemservice.Client

func RpcInit() {
    var err error
    ItemClient, err = itemservice.NewClient("huhangtian.demo.rpc",client.WithHostPorts("127.0.0.1:8888"))
    if err != nil {
        panic(err)
    }
}
