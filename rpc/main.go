package main

import (
	rpc "code.byted.org/demo/rpc/kitex_gen/huhangtian/demo/rpc/itemservice"
	"log"
)

func main() {
	svr := rpc.NewServer(new(ItemServiceImpl))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
