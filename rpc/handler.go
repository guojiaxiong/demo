package main

import (
	"code.byted.org/demo/rpc/kitex_gen/huhangtian/demo/rpc"
	"context"
	"code.byted.org/gopkg/logs"
)

// ItemServiceImpl implements the last service interface defined in the IDL.
type ItemServiceImpl struct{}

// GetItem implements the ItemServiceImpl interface.
func (s *ItemServiceImpl) GetItem(ctx context.Context, req *rpc.GetItemRequest) (resp *rpc.GetItemResponse, err error) {
	// TODO: Your code here...
    logs.CtxDebug(ctx,"GetItem called")
    resp = rpc.NewGetItemResponse()
    resp.Item = rpc.NewItem()
    resp.Item.Id = 1024
    resp.Item.Title = "Hello KiteX!"
    resp.Item.Content = "KiteX is the best framework!"
    return 
}
